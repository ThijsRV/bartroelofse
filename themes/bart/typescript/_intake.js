var intakeform = /** @class */ (function () {
    function intakeform() {
        this._checkbox = $('input:checkbox');
        this._checkfield = $('.checkfield');
        this.addEventListener(event);
    }
    intakeform.prototype.addEventListener = function (event) {
        var _this = this;
        this._checkbox.on("click", function (event) { return _this.handleFieldToggle(event); });
    };
    intakeform.prototype.handleFieldToggle = function (event) {
        if (this._checkfield) {
            var currTarget = $(event.currentTarget);
            if (currTarget.is(':checked')) {
                currTarget.closest('.checkfield').addClass('open');
            }
            else {
                currTarget.closest('.checkfield').removeClass('open');
            }
        }
    };
    return intakeform;
}());
var intake = new intakeform();
//# sourceMappingURL=_intake.js.map