var navigation = /** @class */ (function () {
    function navigation() {
        this._navigation = $('.navigation');
        this._contactBar = $('.contact-bar');
        this._sidenav = $('.navitems');
        this._menuToggle = $('.menutoggle');
        this.addEventListener(event);
    }
    navigation.prototype.addEventListener = function (event) {
        var _this = this;
        $(window).on("scroll", function (event) { return _this.handleNavScroll(); });
        this._menuToggle.on("click", function (event) { return _this.openNav(); });
    };
    navigation.prototype.handleNavScroll = function () {
        if ($(window).scrollTop() > 150) {
            this._navigation.addClass('collapsed');
        }
        else {
            this._navigation.removeClass('collapsed');
        }
    };
    navigation.prototype.openNav = function () {
        var _this = this;
        this._sidenav.toggleClass('open');
        if (this._sidenav.hasClass('open')) {
            $('.logo').css('opacity', 0);
            $(window).on("scroll", function (event) { return _this._sidenav.removeClass('open'); });
        }
        else {
            $('.logo').css('opacity', 1);
        }
    };
    return navigation;
}());
var nav = new navigation();
//# sourceMappingURL=_navigation.js.map