class navigation {

    public _navigation: JQuery;
    public _contactBar: JQuery;
    public _sidenav: JQuery;
    public _menuToggle: JQuery;

    constructor() {

        this._navigation = $('.navigation');
        this._contactBar = $('.contact-bar')
        this._sidenav = $('.navitems');
        this._menuToggle = $('.menutoggle');
        this.addEventListener(event);
    }

    addEventListener(event) {
        $(window).on("scroll", (event) => this.handleNavScroll());
        this._menuToggle.on("click", event => this.openNav());
    }

    handleNavScroll() {

        if ($(window).scrollTop() > 150) {
            this._navigation.addClass('collapsed');
        } else {
            this._navigation.removeClass('collapsed');
        }

    }

    openNav() {
        this._sidenav.toggleClass('open');

        if (this._sidenav.hasClass('open')) {
            $('.logo').css('opacity', 0);

            $(window).on("scroll", event => this._sidenav.removeClass('open'));
        }
        else {
            $('.logo').css('opacity', 1);
        }
    }

}

const nav = new navigation();
