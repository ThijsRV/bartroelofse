class intakeform {

    public _checkbox: JQuery;
    public _checkfield: JQuery;
    constructor() {

        this._checkbox = $('input:checkbox');
        this._checkfield = $('.checkfield');
        this.addEventListener(event);
    }

    addEventListener(event) {
        this._checkbox.on("click", event => this.handleFieldToggle(event));
    }

    handleFieldToggle(event) {
        if (this._checkfield) {
            let currTarget = $(event.currentTarget);

            if (currTarget.is(':checked')) {
                currTarget.closest('.checkfield').addClass('open');
            } else {
                currTarget.closest('.checkfield').removeClass('open');
            }
        }
    }

}

const intake = new intakeform();
