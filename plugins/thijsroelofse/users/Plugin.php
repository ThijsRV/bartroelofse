<?php namespace Thijsroelofse\Users;

use Backend;
use System\Classes\PluginBase;
use Event;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UsersController;
use Input;
use Thijsroelofse\Users\Models\User as ProfileModel;
/**
 * users Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'users',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot(){

        UsersController::extendFormFields(function($form, $model, $contect){

            if(!$model instanceof UserModel) return;

            $form->addTabFields([
                'phonenumber' => [
                    'label' => 'Telefoonnummer',
                    'tab'   => 'Profiel',
                ],

                'dateofbirth' => [
                     'label' => 'Geboortedatum',
                     'tab'   => 'Profiel',
                ],

                'height' => [
                     'label' => 'Lengte',
                     'tab'   => 'Profiel',
                ],

                'weight' => [
                     'label' => 'Gewicht',
                     'tab'   => 'Profiel',
                ],

                'allergics' => [
                     'label' => 'Allergien?',
                     'tab'   => 'Profiel',
                     'type'  => 'textarea',
                ],

                'treatment' => [
                     'label' => 'In behandeling bij arts?',
                     'tab'   => 'Profiel',
                     'type'  => 'textarea',
                ],

                'injuries' => [
                     'label' => 'Blessures?',
                     'tab'   => 'Profiel',
                     'type'  => 'textarea',
                ],
            ]);
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Thijsroelofse\Users\Components\UserRegister' => 'UserRegister',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.users.some_permission' => [
                'tab' => 'users',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'users' => [
                'label'       => 'users',
                'url'         => Backend::url('thijsroelofse/users/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['thijsroelofse.users.*'],
                'order'       => 500,
            ],
        ];
    }
}
