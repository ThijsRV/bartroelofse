<?php namespace Thijsroelofse\Users\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('phonenumber')->nullable();
            $table->string('gender')->nullable();
            $table->string('dateofbirth')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->text('allergics')->nullable();
            $table->text('treatment')->nullable();
            $table->text('injuries')->nullable();
        });
    }

    public function down()
    {
      Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('phonenumber');
            $table->dropColumn('man');
            $table->dropColumn('woman');
            $table->dropColumn('dateofbirth');
            $table->dropColumn('height');
            $table->dropColumn('weight');
            $table->dropColumn('allergics');
            $table->dropColumn('treatment');
            $table->dropColumn('injuries');
      });
    }
}
