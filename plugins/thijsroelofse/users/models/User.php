<?php namespace Thijsroelofse\Users\Models;

use Model;
/**
 * User Model
 */
class User extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'thijsroelofse_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
    ];

    public static function getFromUser($user){

        if($user->profile) return $user->profile;

        $profile = new static;
        $profile->user = $user;
        $profile->save();

        $user->profile = $user;

        return $profile;

    }
}
