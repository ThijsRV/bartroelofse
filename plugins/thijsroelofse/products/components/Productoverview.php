<?php namespace Thijsroelofse\Products\Components;
use Thijsroelofse\Products\Models\Product;
use Cms\Classes\ComponentBase;

class Productoverview extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Productoverview Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['products'] = $this->getProducts();
    }

    public function getProducts(){

        $products = Product::orderBy('price', 'DESC')->get();

        return $products;

    }
}
