<?php namespace Thijsroelofse\Products\Components;

use Cms\Classes\ComponentBase;
use Thijsroelofse\Products\Models\Product;
class Productdetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'productdetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['product'] = $this->getProductDetails();
    }


    public function getProductDetails(){
        $slug = $this->param('product');

        $product = Product::where("slug", $slug)->first();

        if($product == null){
            return;
            //throw 404
        }

        return $product;
    }
}
