<?php namespace Thijsroelofse\Products;

use Backend;
use System\Classes\PluginBase;

/**
 * products Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'products',
            'description' => 'No description provided yet...',
            'author'      => 'thijsroelofse',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Thijsroelofse\Products\Components\Productoverview' => 'Producten',
            'Thijsroelofse\Products\Components\Productdetail' => 'ProductDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'thijsroelofse.products.some_permission' => [
                'tab' => 'products',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'products' => [
                'label'       => 'products',
                'url'         => Backend::url('thijsroelofse/products/products'),
                'icon'        => 'icon-leaf',
                'permissions' => ['thijsroelofse.products.*'],
                'order'       => 500,
            ],
        ];
    }
}
