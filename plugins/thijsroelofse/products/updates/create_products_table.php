<?php namespace Thijsroelofse\Products\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('thijsroelofse_products_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->json('product_properties');
            $table->text('product_description');
            $table->text('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('thijsroelofse_products_products');
    }
}
