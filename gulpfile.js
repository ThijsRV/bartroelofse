'use strict';

// Requirements
var gulp = require('gulp'),
    clean = require('gulp-clean'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    sassLint = require('gulp-sass-lint'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    typescript = require('gulp-tsc'),
    runSequence = require('run-sequence');

// Paths
var DEV = 'themes/bart',
    STYLES = '/scss',
    SCRIPTS = '/typescript';

// Default task
gulp.task('default', function () {
    runSequence('clean', ['sass', 'typescript']);
});

// Watch task
gulp.task('watch', function () {
    gulp.watch(DEV + STYLES + '/**/*.scss', ['sass']);
    gulp.watch(DEV + SCRIPTS + '/**/*.ts', ['typescript']);
});

// Clean task
gulp.task('clean', function () {
    return gulp.src([DEV + '/assets/css'], { read: false })
        .pipe(clean());
});

// Typescript task
gulp.task('typescript', function () {
    return gulp.src(DEV + SCRIPTS + '/**/*.ts')
        .pipe(plumber())
        .pipe(typescript({
            module: 'amd',
            target: 'ES6',
            inlineSourceMap: true,
            inlineSources: true,
            out: 'core.js'
        }))
        //.pipe(uglify())
        .pipe(plumber.stop())
        .pipe(gulp.dest(DEV + '/assets/js'));
});

// Sass task
gulp.task('sass', function () {
    return gulp.src(DEV + STYLES + '/core.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({ browsers: ['last 2 versions', '> 2%'], cascade: false }))
        .pipe(rename('core.css'))
        .pipe(cssnano())
        .pipe(gulp.dest(DEV + '/assets/css'));
});
